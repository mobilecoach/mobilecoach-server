#!/bin/bash

echo "Running this script will remove the changes made in the contents of the folder."
echo "Are you sure you don't have any improtant data in this folder? (Answer with 'yes')"
read -e ANSWER
if [ ! "$ANSWER" = "yes" ]
  then
  echo "Please restart the script to enter the information again."
  exit 0
fi
git clean -fd
git reset --hard

echo "Please enter the hostname of the server (example: workshop-cdhi.ethz.ch):"
read -e domain

echo "Please enter the tomcat port (example: 8443):"
read -e tomcat_port

echo "Please enter the deepstream port (example: 6020):"
read -e deepstream_port

echo "Please enter your email id for letsencrypt notices (example: abcd@ef.gh):"
read -e email

echo "The informations provided are hostname: $domain, tomcat port: $tomcat_port, deepstream port: $deepstream_port and email: $email ."
echo "Are you sure of these details, and used this script with 'sudo' and want to continue setup? If okay, please answer with 'yes':"
read -e ANSWER

if [ ! "$ANSWER" = "yes" ]
  then
  echo "Please restart the script to enter the information again."
  exit 0
fi

certbot --standalone certonly --config-dir letsencrypt/config --work-dir letsencrypt/work --logs-dir letsencrypt/logs --non-interactive --agree-tos -m $email -d $domain

keystorepass=`openssl rand -hex 8`

echo "Please copy and use $keystorepass as export password below."

openssl pkcs12 -export -in ./letsencrypt/config/live/${domain}/fullchain.pem -inkey ./letsencrypt/config/live/${domain}/privkey.pem -name tomcat -out certs/cert.p12 -CAfile \
 ./letsencrypt/config/live/${domain}/chain.pem -caname root

keytool -importkeystore -deststorepass $keystorepass -destkeypass $keystorepass -destkeystore certs/keystore_tomcat -srckeystore certs/cert.p12 -srcstoretype PKCS12 -srcstorepass $keystorepass -alias tomcat

keytool -importkeystore -srckeystore certs/keystore_tomcat -destkeystore certs/keystore_tomcat -deststoretype pkcs12 -srcstorepass $keystorepass

find . -type f -exec sed -i 's/<hostname>/'"$domain"'/g' {} +
find . -type f -exec sed -i 's/<port_tomcat>/'"$tomcat_port"'/g' {} +
find . -type f -exec sed -i 's/<port_deepstream>/'"$deepstream_port"'/g' {} +
find . -type f -exec sed -i 's/<password_keystore>/'"$keystorepass"'/g' {} +

