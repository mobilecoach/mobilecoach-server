certbot --standalone renew --config-dir letsencrypt/config --work-dir letsencrypt/work --logs-dir letsencrypt/logs

export domain=<hostname>
export keystorepass=<password_keystore>

echo "Please copy and enter the export password whenever asked. The export password is: $keystorepass"

openssl pkcs12 -export -in ./letsencrypt/config/live/${domain}/fullchain.pem -inkey ./letsencrypt/config/live/${domain}/privkey.pem -name tomcat -out ./certs/cert.p12 -CAfile ./letsencrypt/config/live/${domain}/chain.pem -caname root

cp ./certs/keystore_tomcat ./certs/keystore_tomcat.backup

keytool -importkeystore -deststorepass $keystorepass -destkeypass $keystorepass -destkeystore certs/keystore_tomcat -srckeystore ./certs/cert.p12 -srcstoretype PKCS12 -srcstorepass $keystorepass -alias tomcat

echo "Please copy and enter the export password whenever asked. The keystore password is: $keystorepass"

keytool -importkeystore -srckeystore ./certs/keystore_tomcat -destkeystore ./certs/keystore_tomcat -deststoretype pkcs12

echo "Restarting the docker containers.."

docker-compose restart
