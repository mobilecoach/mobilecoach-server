#!/bin/bash

read -p "Do you want to clean everything, including the database (y/n)? " CONT
if [ "$CONT" != "y"  -a "$CONT" != "Y" ]; then
  exit;
fi

cd mc_global/mc_data/
rm -r -f FileStorage/ MediaCache/ MediaUpload/ logs/ templates/
cd ..
cd mongo_data/
rm -r -f *
cd ..
cd ..
